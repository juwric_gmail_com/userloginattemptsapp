﻿using Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data
{
    public class Seed
    {
        public static async Task SeedData(StoreDbContext context, UserManager<User> userManager)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            var users = new List<User>
            {
                new User
                {
                    Id  = Guid.NewGuid().ToString(),
                    Name = "Bob",
                    Surname = "bob",
                    UserName = "bob",
                    Email = "bob@test.com"
                },
                new User
                {
                    Id  = Guid.NewGuid().ToString(),
                    Name = "Jane",
                    Surname = "jane",
                    UserName = "jane",
                    Email = "jane@test.com"
                },
                new User
                {
                    Id  = Guid.NewGuid().ToString(),
                    Name = "Tom",
                    Surname = "tom",
                    UserName = "tom",
                    Email = "tom@test.com"
                },
            };

            foreach (var user in users)
            {
                await userManager.CreateAsync(user, "Pa$$w0rd");
            }


            var attempts = new List<UserLoginAttempt>();
            var rnd = new Random();


            for (int i = 0; i < 2000; i++)
            {
                attempts.Add(new UserLoginAttempt
                {
                    AttemptTime = new DateTime(rnd.Next(2010, 2022), rnd.Next(1, 12), rnd.Next(1, 12), rnd.Next(1, 24), 0, 0),
                    IsSuccess = (i % 2 == 0) ? true : false,
                    UserId = users[rnd.Next(0, users.Count)].Id
                });
            }

            await context.Attempt.AddRangeAsync(attempts);
            await context.SaveChangesAsync();
        }
    }
}
