﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Models
{
    public class User : IdentityUser
    {
        [Required]
        [EmailAddress]
        [StringLength(maximumLength: 25)]
        public override string Email { get; set; }

        [Required]
        [StringLength(maximumLength: 30)]
        public string Name { get; set; }

        [Required]
        [StringLength(maximumLength: 30)]
        public string Surname { get; set; }

        public List<UserLoginAttempt> list = new List<UserLoginAttempt>();
    }
}
