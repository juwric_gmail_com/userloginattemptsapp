﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data.Models
{
    public class UserLoginAttempt : BaseEntity
    {
        [Required]
        public DateTime AttemptTime { get; set; }

        [Required]
        public bool IsSuccess { get; set; }

        public string UserId { get; set; }
        public virtual User user { get; set; }
    }
}
