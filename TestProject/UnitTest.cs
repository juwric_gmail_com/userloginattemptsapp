using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UserLoginAttemptsApp.Controllers;
using UserLoginAttemptsApp.Util;
using Xunit;

namespace TestProject
{
    public class UnitTest
    {
        [Fact]
        public void TestHomeControllerIndex()
        {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = controller.Index();

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public async Task TestHomeControllerGetByEmailAsync()
        {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = await controller.GetByEmail("");

            // Assert
            Assert.Null(result.Value);
        }
    }
}
