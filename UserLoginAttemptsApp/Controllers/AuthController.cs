﻿using Application.ApiModel;
using Application.Command;
using AutoMapper;
using Data;
using Data.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using UserLoginAttemptsApp.Util;

namespace UserLoginAttemptsApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly IConfiguration configuration;

        public AuthController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
        }

        [HttpPost("register")]
        public async Task<ActionResult<AuthenticationResponse>> Register([FromBody] UserCreateCommand createCommand)
        {
            var user = new IdentityUser { UserName = createCommand.Name, Email = createCommand.Email };
            var result = await userManager.CreateAsync(user, createCommand.Password);

            if (result.Succeeded)
            {
                return await Security.BuildToken(createCommand.Email, userManager, configuration["keyjwt"]);
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }

        [HttpPost("login")]
        public async Task<ActionResult<AuthenticationResponse>> Login([FromBody] UserLoginCommand loginCommand)
        {
            var result = await signInManager.PasswordSignInAsync(loginCommand.Email, loginCommand.Password, isPersistent: false, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                return await Security.BuildToken(loginCommand.Email, userManager, configuration["keyjwt"]);
            }
            else
            {
                return BadRequest("Incorrect Login");
            }
        }

        [HttpGet("logout")]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return NoContent();
        }

        [HttpPost("me")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IdentityUser> Me()
        {
            var email = HttpContext.User.Claims.FirstOrDefault(x => x.Type == "email")?.Value;
            if (email == null)
            {
                return new IdentityUser();
            }

            return await userManager.FindByEmailAsync(email);
        }
    }
}
