﻿using Application.ApiModel;
using Application.ApiModels;
using Application.Contract;
using AutoMapper;
using Data;
using Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UserLoginAttemptsApp.Util;

namespace UserLoginAttemptsApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly StoreDbContext _context;
        private readonly UserManager<User> _userManager;
        private readonly IAttemptRepository _attemptRepository;
        private readonly IMapper mapper;

        public HomeController()
        {}

        public HomeController(StoreDbContext context, UserManager<User> userManager, IAttemptRepository attemptRepository, IMapper mapper)
        {
            this._context = context;
            this._userManager = userManager;
            this._attemptRepository = attemptRepository;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return Content("HomeController");
        }

        [HttpGet("Init")]
        public async Task<IActionResult> Init()
        {
            await Seed.SeedData(_context, _userManager);
            return NoContent();
        }

        [HttpGet("GetByEmail")]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = 300)]
        public async Task<ActionResult<TotalMetricWithUser>> GetByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return BadRequest("email not valid");
            }

            User user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return BadRequest("user not exists");
            }

            var all = await _attemptRepository.GetByUserId(user.Id);
            if (all.Count > 0)
            {
                TotalMetricWithUser result = GenerateMetric.Generate(all);
                result.User = mapper.Map<UserDTO>(user);
                return result;
            }

            return NoContent();
        }

        [HttpGet("Statistic")]
        public async Task<ActionResult<Object>> Statistic(DateTime? startDate, DateTime? endDate, MetricUnit? metric, bool? IsSuccess)
        {
            var all = await _attemptRepository.GetByParameters(startDate, endDate, IsSuccess);
            if (all.Count > 0)
            {
                TotalMetric result = GenerateMetric.Generate(all, metric);
                return result;
            }

            return NoContent();
        }
    }
}
