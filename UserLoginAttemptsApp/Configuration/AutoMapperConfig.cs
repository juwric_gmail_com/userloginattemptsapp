﻿using Application.ApiModels;
using AutoMapper;
using Data.Models;

namespace UserLoginAttemptsApp.Configuration
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<User, UserDTO>();
        }
    }
}
