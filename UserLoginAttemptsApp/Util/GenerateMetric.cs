﻿using Application.ApiModel;
using Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace UserLoginAttemptsApp.Util
{
    public class GenerateMetric
    {
        public static TotalMetricWithUser Generate(IEnumerable<UserLoginAttempt> attempts, MetricUnit? metric = null)
        {
            IEnumerable<Metric> hour = null;
            IEnumerable<Metric> month = null;
            IEnumerable<Metric> year = null;

            if (metric == null || metric == MetricUnit.hour)
            {
                hour = attempts.GroupBy(p => p.AttemptTime.ToString("yyyy-MM-dd HH:mm:ss")).Select(g => new Metric
                {
                    Period = g.Key,
                    Value = g.Count(),
                }).ToArray();
            }

            if (metric == null || metric == MetricUnit.month)
            {
                month = attempts.GroupBy(p => p.AttemptTime.ToString("MMMM, yyyy")).Select(g => new Metric
                {
                    Period = g.Key,
                    Value = g.Count(),
                }).ToArray();
            }

            if (metric == null || metric == MetricUnit.year)
            {
                year = attempts.GroupBy(p => p.AttemptTime.ToString("yyyy")).Select(g => new Metric
                {
                    Period = g.Key,
                    Value = g.Count(),
                }).ToArray();
            }

            TotalMetricWithUser result = new TotalMetricWithUser()
            {
                Hour = hour,
                Month = month,
                Year = year
            };

            return result;
        }
    }
}
