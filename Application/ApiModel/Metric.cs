﻿namespace Application.ApiModel
{
    public class Metric
    {
        public string Period { get; set; }
        public int Value { get; set; }
    }
}
