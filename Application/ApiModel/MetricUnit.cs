﻿namespace Application.ApiModel
{
    public enum MetricUnit
    {
        hour,
        month,
        year
    }
}
