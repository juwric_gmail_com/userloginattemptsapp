﻿using Application.ApiModels;

namespace Application.ApiModel
{
    public class TotalMetricWithUser : TotalMetric
    {
        public UserDTO User { get; set; }
    }
}
