﻿using System.Collections.Generic;

namespace Application.ApiModel
{
    public class TotalMetric
    {
        public IEnumerable<Metric> Hour { get; set; }
        public IEnumerable<Metric> Month { get; set; }
        public IEnumerable<Metric> Year { get; set; }
    }
}
