﻿using Application.Contract;
using Data;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Repository
{
    public class AttemptRepository : IAttemptRepository
    {
        private readonly StoreDbContext _context;

        public AttemptRepository(StoreDbContext context)
        {
            this._context = context;
        }

        public async Task<List<UserLoginAttempt>> Get()
        {
            return await _context.Attempt.Include(a => a.user)
                .AsNoTracking().ToListAsync();
        }

        //public async Task<List<UserLoginAttempt>> GetByParameters(DateTime? startDate, DateTime? endDate, MetricUnit? metric, bool? IsSuccess)
        //{
        //    return await _context.Attempt.Include(a => a.user)
        //        .AsNoTracking().ToListAsync();
        //}

        public async Task<List<UserLoginAttempt>> GetByUserId(string id)
        {
            return await _context.Attempt.Where(u => u.UserId == id)
                .AsNoTracking().ToListAsync();
        }

        public async Task<List<UserLoginAttempt>> GetByParameters(DateTime? startDate, DateTime? endDate, bool? isSuccess)
        {
            IQueryable<UserLoginAttempt> set = _context.Attempt;

            if (startDate != null)
            {
                set = set.Where(a => a.AttemptTime > startDate);
            }

            if (endDate != null)
            {
                set = set.Where(a => a.AttemptTime < endDate);
            }

            if (isSuccess != null)
            {
                set = set.Where(a => a.IsSuccess == isSuccess);
            }

            return await set.AsNoTracking().ToListAsync();
        }
    }
}
