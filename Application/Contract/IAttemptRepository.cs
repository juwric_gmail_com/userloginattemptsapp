﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Contract
{
    public interface IAttemptRepository
    {
        Task<List<UserLoginAttempt>> Get();
        Task<List<UserLoginAttempt>> GetByUserId(string id);
        Task<List<UserLoginAttempt>> GetByParameters(DateTime? startDate, DateTime? endDate, bool? isSuccess);
    }
}
