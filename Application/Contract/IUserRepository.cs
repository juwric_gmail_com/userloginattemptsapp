﻿using Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Contract
{
    public interface IUserRepository
    {
        Task<List<User>> Get();
    }
}
